<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CakeController;
use App\Http\Controllers\InterestedEmailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'cake'],function(){

    Route::post('/create',[CakeController::class,'create']);
    Route::put('/update/{id}',[CakeController::class,'update']);
    Route::get('/list/{id}',[CakeController::class,'show']);
    Route::get('/all',[CakeController::class,'showAll']);
    Route::delete('/delete/{id}',[CakeController::class,'delete']);

});

Route::group(['prefix' => 'email'],function(){

    Route::post('/create',[InterestedEmailController::class,'create']);
    Route::put('/update/{id}',[InterestedEmailController::class,'update']);
    Route::get('/list/{id}',[InterestedEmailController::class,'show']);
    Route::get('/all',[InterestedEmailController::class,'showAll']);
    Route::delete('/delete/{id}',[InterestedEmailController::class,'delete']);

});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return 'teste';
//     return $request->user();
// });
