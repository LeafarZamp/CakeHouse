<?php

namespace App\Http\Controllers;

use App\Models\Cake;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CakeController extends Controller
{

    protected $createRules = [
        'name'      => 'required|string|max:255',
        'weight'    => 'required|numeric',
        'price'     => 'required|numeric',
        'quantity'  => 'required|numeric'
    ];

    public function create(Request $request){

        $validator = Validator::make($request->all(), $this->createRules);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'data' => $validator->errors()
            ], 422);
        }

        $cake = Cake::create([
            "name"      => $request['name'],
            "weight"    => $request['weight'],
            "price"     => $request['price']*100,
            "quantity"  => $request['quantity']
        ]);

        $cake->price_mask = number_format($cake->price/100,2,',','.');
        return response()->json([
            'status' => 'success',
            'data' => $cake
        ], 200);
    }

    public function update(Request $request){

        $cake = Cake::find($request->id);

        if($cake){

            $cake->update([
                "name"      => $request['name'],
                "weight"    => $request['weight'],
                "price"     => $request['price']*100,
                "quantity"  => $request['quantity']
            ]);

            $cake->price_mask =  number_format($cake->price/100,2,',','.');

            return response()->json([
            'status' => 'success',
            'data' => $cake
        ], 200);

        }else{

            return response()->json([
            'status' => 'failed',
            'message' => 'bolo não encontrado'
        ], 200);

    }

}

    public function delete(Request $request){

        $cake = Cake::find($request->id);

        if($cake){

            $cake->delete();

            return response()->json([
            'status' => 'success',
            'message' => 'bolo excluido com sucesso'
        ], 200);

        }else{

            return response()->json([
            'status' => 'failed',
            'message' => 'bolo não encontrado'
        ], 200);

    }

}

    public function show(Request $request){

        $cake = Cake::find($request->id);

        if($cake){

            $cake->price_mask =  number_format($cake->price/100,2,',','.');

            return response()->json([
            'status' => 'success',
            'data' => $cake
        ], 200);

    }else{

            return response()->json([
            'status' => 'failed',
            'message' => 'bolo não encontrado'
        ], 200);

    }

}

public function showAll(){

    $cake = Cake::all();

    if(count($cake) > 0){

        for ($i=0; $i < count($cake); $i++) {

            $cake[$i]->price_mask =  number_format($cake[$i]->price/100,2,',','.');

        }

        return response()->json([
        'status' => 'success',
        'data' => $cake
    ], 200);

}else{

        return response()->json([
        'status' => 'failed',
        'message' => 'nenhum bolo cadastrado no sistema'
    ], 200);

}

}
}
