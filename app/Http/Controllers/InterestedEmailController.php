<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Mail\WelcomeEmail;
use App\Models\Cake;
use App\Models\interestedEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class InterestedEmailController extends Controller
{
    protected $createRules = [
        'name'      => 'required|string|max:255',
        'email'     => 'required|string|email|unique:interested_emails,email'
    ];

    public function create(Request $request){


        $validator = Validator::make($request->all(), $this->createRules);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'data' => $validator->errors()
            ], 422);
        }

        $email = interestedEmail::create([
            "name"      => $request['name'],
            "email"     => $request['email']
        ]);

        $cake = Cake::where('quantity','>',0)->get();

        for ($i=0; $i < count($cake); $i++) {

            $cake[$i]->price_mask =  number_format($cake[$i]->price/100,2,',','.');

        }

        if(count($cake) > 0){

            $data = new \stdClass();

            $data->title    = 'Bem Vindo a Casa de Bolos!';
            $data->info     =  'Veja o que temos disponível no momento';
            $data->cakes = $cake;
            $data->name = $email->name;

            SendEmail::dispatch($email->email, new WelcomeEmail($data));

        }

        return response()->json([
            'status' => 'success',
            'data' => $email
        ], 200);
    }

    public function update(Request $request){


        $email = interestedEmail::find($request->id);

        if($email){

            $email->update([
                "name"      => $request['name'],
                "email"    => $request['email']
            ]);


            return response()->json([
            'status' => 'success',
            'data' => $email
        ], 200);

        }else{

            return response()->json([
            'status' => 'failed',
            'message' => 'email não encontrado'
        ], 200);

    }

}

    public function delete(Request $request){

        $email = interestedEmail::find($request->id);

        if($email){

            $email->delete();

            return response()->json([
            'status' => 'success',
            'message' => 'email excluido com sucesso'
        ], 200);

        }else{

            return response()->json([
            'status' => 'failed',
            'message' => 'email não encontrado'
        ], 200);

    }

}

    public function show(Request $request){

        $email = interestedEmail::find($request->id);

        if($email){

            return response()->json([
            'status' => 'success',
            'data' => $email
        ], 200);

    }else{

            return response()->json([
            'status' => 'failed',
            'message' => 'email não encontrado'
        ], 200);

    }

}

public function showAll(){

    $email = interestedEmail::all();

    if(count($email) > 0){

        return response()->json([
        'status' => 'success',
        'data' => $email
    ], 200);

}else{

        return response()->json([
        'status' => 'failed',
        'message' => 'nenhum email cadastrado no sistema'
    ], 200);

}

}
}
