<?php

namespace App\Observers;

use App\Jobs\SendEmail;
use App\Mail\CakeEmail;
use App\Models\Cake;
use App\Models\interestedEmail;

class CakeObserver
{
    /**
     * Handle the Cake "created" event.
     *
     * @param  \App\Models\Cake  $cake
     * @return void
     */
    public function created(Cake $cake)
    {
        $email = interestedEmail::all();

        if($cake->quantity >0){

        foreach($email as $row){

            $data = new \stdClass();

            $data->title    = 'Novo sabor está a sua espera!';
            $data->info     =  'Um novo sabor está a sua espera!';
            $data->name     = $row->name;

            $cake->price_mask =  number_format($cake->price/100,2,',','.');
            $data->cakes    = $cake;

        SendEmail::dispatch($row->email, new CakeEmail($data));

        }
    }

}

    /**
     * Handle the Cake "updated" event.
     *
     * @param  \App\Models\Cake  $cake
     * @return void
     */
    public function updated(Cake $cake)
    {
        $email = interestedEmail::all();

        if($cake->quantity >0){

            foreach($email as $row){

                $data = new \stdClass();

                $data->title   = 'Olha só o que temos disponível!';
                $data->info   = 'Olha só o que temos disponível!';

                $data->name = $row->name;
                $cake->price_mask =  number_format($cake->price/100,2,',','.');

                $data->cakes = $cake;


            SendEmail::dispatch($row->email, new CakeEmail($data));

            }
        }

    }

    /**
     * Handle the Cake "deleted" event.
     *
     * @param  \App\Models\Cake  $cake
     * @return void
     */
    public function deleted(Cake $cake)
    {
        //
    }

    /**
     * Handle the Cake "restored" event.
     *
     * @param  \App\Models\Cake  $cake
     * @return void
     */
    public function restored(Cake $cake)
    {
        //
    }

    /**
     * Handle the Cake "force deleted" event.
     *
     * @param  \App\Models\Cake  $cake
     * @return void
     */
    public function forceDeleted(Cake $cake)
    {
        //
    }
}
