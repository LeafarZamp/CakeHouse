<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Email Casa de Bolo</title>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            min-width: 100%!important;
        }

        img {
            height: auto;
        }

        .content {
            width: 100%;
            max-width: 600px;
        }

        .header {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 150px;
            border-bottom: 1px solid #f5f5f5f6;
            height: 160px;
        }

        .innerpadding {
            padding: 10px 30px 1px 30px;
        }

        .borderbottom {
            border-bottom: 1px solid #f2eeed;
        }

        .subhead {
            padding: 0 0 0 3px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .subhead img {
            width: 60%;
        }

        .h1,
        .h2,
        .bodycopy {
            font-size: 16px;
            line-height: 22px;
            text-align: center;
            font-family: sans-serif;
        }

        .h1 {
            font-size: 33px;
            line-height: 38px;
            font-weight: bold;
        }

        .h2 {
            padding: 0 0 15px 0;
            font-size: 24px;
            line-height: 28px;
            font-weight: bold;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .bodycopy {
            font-size: 16px;
            line-height: 22px;
        }

        .button {
            text-align: center;
            font-size: 18px;
            font-family: sans-serif;
            font-weight: bold;
            padding: 0 30px 0 30px;
            border-radius: 15px;
        }

        .button a {
            color: #ffffff;
            text-decoration: none;
        }

        .footer {
            padding: 20px 30px 15px 30px;
        }

        .footercopy {
            font-family: sans-serif;
            font-size: 14px;
            color: #ffffff;
            margin-top: 2rem !important;
        }

        .footercopy a {
            color: #ffffff;
            text-decoration: underline;
        }

        .titulo {
            color: #333399;
        }

        .color {
            color: rgb(90, 90, 90);
            font-size: 1.5em;
            font-weight: 500;
            display: flex;
            justify-content: center;
            align-items: center;
            line-height: 25px;
        }

        .buttonwrapper {
            border-radius: 25px;
        }

        .tabela-email {
            height: 100vh;
            justify-content: center;
            align-items: center;
        }

        .tabela-border {
            border: 1px solid #dcdcdc;
        }

        .img-welcome {
            width: 400px;
        }

        .img-redesocial {
            width: 35px;
        }

        @media only screen and (max-width: 550px),
        screen and (max-device-width: 550px) {
            body[yahoo] .hide {
                display: none!important;
            }
            body[yahoo] .buttonwrapper {
                background-color: transparent!important;
            }
            body[yahoo] .button {
                padding: 0px!important;
            }
            body[yahoo] .button a {
                background-color: #e05443;
                padding: 15px 15px 13px!important;
            }
            body[yahoo] .unsubscribe {
                display: block;
                margin-top: 20px;
                padding: 10px 50px;
                background: #2f3942;
                border-radius: 5px;
                text-decoration: none!important;
                font-weight: bold;
            }
        }
    </style>
</head>
<body bgcolor="#f6f8f1">
<table class="tabela-email" width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table bgcolor="#ffffff" class="content tabela-border" align="center" cellpadding="0" cellspacing="0" >
                <tr>
                    <td bgcolor="#ffffff" style="border-left: 1px solid #dcdcdc; border-right: 1px solid #dcdcdc">
            <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%;text-align: center;">
    <tr>
        <td height="70">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td class="subhead" style="padding: 0 0 0 3px;">
                            <img src="https://img.icons8.com/plasticine/100/000000/cake.png" style="width: 30%; margin: 0 auto;"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
 </td>

</tr>
                <tr>
                    <td class="innerpadding">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="h2" style="text-align: center;width: 100%; display: inline-block">
                                        Olá, {{$data->name}}
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="innerpadding" style="padding-top:1rem;padding-bottom:3rem;color: #868686;text-align: center;">
                        <table class="col380" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; display: inline-block">
                            <tbody>

                                <tr style="display: inline-block;width: 100%">
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td class="bodycopy" style="text-align: left;color:black">
                                                    <br>
                                                    {{$data->info}}
                                                    <br>

                                                </td>
                                            </tr>

                                                <tr>
                                                <td class="bodycopy" style="text-align: left">
                                                    <br>
                                                    {{$data->cakes['name']}} {{$data->cakes['weight']}} gramas por apenas R$ {{$data->cakes['price_mask']}}.
                                                    <br>

                                                </td>

                                            </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="footer" bgcolor="#f50272">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" style="padding: 15px 0 23px 0;">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                        <a style="color: white;" href="" target="_blank">
                            <img class="img-redesocial" src="https://img.icons8.com/ios/50/000000/facebook-circled--v1.png"/>
                        </a>
                    </td>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                        <a style="color: white;" href="" target="_blank">
                            <img class="img-redesocial" src="https://img.icons8.com/ios/50/000000/instagram-new--v1.png"/>
                        </a>
                    </td>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                        <a style="color: white;" href="" target="_blank">
                            <img class="img-redesocial" src="https://img.icons8.com/ios/50/000000/whatsapp--v1.png"/>
                        </a>
                    </td>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                        <a style="color: white;" href="mailto:CasaBolos@teste.com" target="_top">
                            <img class="img-redesocial" src="https://img.icons8.com/ios/50/000000/composing-mail.png"/>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" class="footercopy">
            CASA DE BOLOS &#169; {{ now()->year }}
        </td>

    </tr>
</table>
</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>
